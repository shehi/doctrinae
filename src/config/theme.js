// @ts-nocheck
import { createTheme } from "@mui/material/styles";
const WB_BLACK = "#222732";

// Custom WB Theme
const theme = createTheme(
  {
    typography: {
      h1: {
        color: WB_BLACK,
      },
      h2: {
        color: WB_BLACK,
      },
      h3: {
        color: WB_BLACK,
      },
      h4: {
        color: WB_BLACK,
      },
      h5: {
        color: WB_BLACK,
      },
      h6: {
        color: WB_BLACK,
      },
      subtitle1: {
        color: WB_BLACK,
      },
      subtitle2: {
        color: WB_BLACK,
      },
      body1: {
        color: WB_BLACK,
      },
      body2: {
        color: WB_BLACK,
      },
      caption: {
        color: WB_BLACK,
      },
      button: {
        color: WB_BLACK,
      },
      overline: {
        color: WB_BLACK,
      },
      fontFamily: "Roboto",
    },
    components: {
      MuiTypography: {
        variants: [
          {
            props: { type: "grey" },
            style: {
              opacity: 0.6,
            },
          },
          {
            props: { type: "gradient" },
            style: {
              background: "#DC331B",
              backgroundImage:
                "linear-gradient(to right, #DC331B 0%, #E36415 60%)",
              backgroundClip: "text",
              WebkitBackgroundClip: "text",
              color: "transparent",
            },
          },
        ],
      },
      MuiDataGrid: {
        styleOverrides: {
          columnSeparator: {
            display: "none",
          },
        },
      },
    },
    palette: {
      common: {
        white: "#ffffff",
        black: "#000000",
      },
      primary: {
        main: "#DC331B",
      },
      secondary: {
        main: "#FF8E53",
      },
      error: {
        main: "#FF0000",
      },
    },
  },
  {
    doctrinae: {
      background: "#F9F9F9",
      textColor: "#222732",
      white: "#ffffff",
    },
  }
);

export default theme;
